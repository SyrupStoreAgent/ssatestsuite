/**
 * Created by dc on 2015. 8. 6..
 */


angular.module('app', []);

angular.module('app').controller('appController', posController);


function posController($scope, Connector) {

	// Protocol Version Settings
	$scope.protocolVersion = "1.1.0";
	$scope.schemeVersion = "1.2";


	$scope.msg2Ssa = "";
    $scope.responses = [];
	$scope.maxResponse = 2;
	$scope.refreshEnabled = true;

	$scope.makeTid = function () {
		var date = new Date();
		var tid = 'POS' + date.getFullYear() + ('00' + (date.getMonth() + 1)).slice(-2) +
			('00' + date.getDate()).slice(-2) + ('00' + date.getHours()).slice(-2) +
			('00' + date.getMinutes()).slice(-2) + ('00' + date.getSeconds()).slice(-2) +
			date.getMilliseconds();

		return tid;
	};

	$scope.makePosTid = function () {
		var date = new Date();
		var tid = 'pos_device_tr_key_' + date.getFullYear() + ('00' + (date.getMonth() + 1)).slice(-2) +
			('00' + date.getDate()).slice(-2) + ('00' + date.getHours()).slice(-2) +
			('00' + date.getMinutes()).slice(-2) + ('00' + date.getSeconds()).slice(-2) +
			date.getMilliseconds();

		return tid;
	};



	$scope.processMessageFromSsa = function(msg) {
		var obj = null;
		try { 
			obj = JSON.parse(msg);
		}catch(ex) { 
			$scope.pushResponseMessage(msg);
			return;
		}
		
		if(obj.header.opCode.type === 'REQ' && obj.header.opCode.command === 'CM_ALIVE') { 
			$scope.processCmAlive(obj);
			return;
		}
		
		if(obj.header.opCode.type === 'REQ' && obj.header.opCode.command === 'CM_EXECUTE') { 
			$scope.processCmExecute(obj);
			return;
		}

		$scope.pushResponseMessage(msg);
	};
	
	
	$scope.pushResponseMessage = function(msg) { 
		if($scope.refreshEnabled) {

			if($scope.responses.length === $scope.maxResponse) {
				$scope.responses.splice($scope.maxResponse - 1, 1);
			}
			$scope.responses.splice(0, 0, msg);
		}
	};
	
	
	$scope.processCmAlive = function(msg) { 
		var resp = {};
		
		resp = angular.copy(msg);
		
		resp.header.source = 'pos';
		resp.header.destination = 'ssa';
		resp.header.opCode.type = 'RES';
		resp.header.responseCode = { code : 0};

		
		$scope.sendMsg2Ssa(JSON.stringify(resp));	
	
	};
	
	$scope.processCmExecute = function(msg) { 
		var resp = {};
		
		resp = angular.copy(msg);
		
		resp.header.source = 'pos';
		resp.header.destination = 'ssa';
		resp.header.opCode.type = 'RES';
		resp.header.responseCode = {code : 0} ;
		resp.body = {
			pdu : {
				schemeVersion : $scope.schemeVersion,
				method : 'GET',
				scheme : 'pos/*/status',
				contents : {
					vendor : 'hello/world',
					protocolVersion : '1.1.0',
					schemeVersion : '1.2',
					salesStatus : 'open'
				}
			},
			extra : { 
			}
		};
		$scope.sendMsg2Ssa(JSON.stringify(resp));	
	
	};
	
	
	$scope.eableRefresh = function(bEnable) { 
		$scope.refreshEnabled = bEnable;
	};
	
	
	$scope.sendInputMsg = function() { 
		if(!$scope.msg2Ssa.length)
			return;
		
		$scope.sendMsg2Ssa($scope.msg2Ssa);
	};
	
	$scope.sendMsg2Ssa = function(msg) { 

		$scope.msg2Ssa = msg;
		Connector.send(msg);
        //var obj = document.getElementById("PosCorrespondentCtrl");
        //obj.PostMessageToSyrupStore(msg);
	};
	
	$scope.setInputStartSales = function() {
		var packet = {
			header : {
				transactionNumber: $scope.makeTid(),
				protocolVersion: $scope.protocolVersion,
				source: "pos",
				destination: "ssa",
				contentType: "",
				opCode: {
					type: "REQ",
					command: "CM_EXECUTE"
				}
			},
			body: {
				pdu: {
					schemeVersion: "1.2",
					method : "PUT",
					scheme : "pos/sales",
					contents: {
						saleStatus: "open"
					}
				},
				extra: { }
			}
		};

		$scope.msg2Ssa = JSON.stringify(packet);
	};
	
	$scope.setInputSaveUsePoint = function() {
		var packet = {
			header : {
				transactionNumber: $scope.makeTid(),
				protocolVersion: $scope.protocolVersion,
				source: "pos",
				destination: "ssa",
				contentType: "",
				opCode: {
					type: "REQ",
					command: "CM_SERVICE"
				}
			},
			body: {
				pdu: {
					deviceTrKey: $scope.makePosTid(),
					serviceCode: "SKP0000",
					serviceType: "common",
					authType: "skip",
					authValue: "skip",
					priceInfo: {
						priceA: 12000
					}
				},
				extra: { }
			}
		};

		$scope.msg2Ssa = JSON.stringify(packet);
	};
	
	$scope.setInputCancelTrade = function() {
		var packet = {
			header : {
				transactionNumber: $scope.makeTid(),
				protocolVersion: $scope.protocolVersion,
				source: "pos",
				destination: "ssa",
				contentType: "",
				opCode: {
					type: "REQ",
					command: "CM_SERVICE"
				}
			},
			body: {
				pdu: {
					deviceTrKey: $scope.makePosTid(),
					serviceTrKey: "999591843897",
					serviceCode: "SKP2000",
					serviceType: "cancel"
				},
				extra: { }
			}
		};

		$scope.msg2Ssa = JSON.stringify(packet);
	};
	
	$scope.setInputStopSales = function() {
		var packet = {
			header : {
				transactionNumber: $scope.makeTid(),
				protocolVersion: $scope.protocolVersion,
				source: "pos",
				destination: "ssa",
				contentType: "",
				opCode: {
					type: "REQ",
					command: "CM_EXECUTE"
				}
			},
			body: {
				pdu: {
					schemeVersion: "1.2",
					method : "PUT",
					scheme : "pos/sales",
					contents: {
						saleStatus: "close"
					}
				},
				extra: { }
			}
		};

		$scope.msg2Ssa = JSON.stringify(packet);
	};

	Connector.setListener(function(data) {
		$scope.processMessageFromSsa(data);
		$scope.$apply();
		return false;
	});
}



function prepareActiveXCom()
{
	var ctrl = document.getElementById("PosCorrespondentCtrl");
	ctrl.PrepareCommunication();
}

function sendMsgThruActiveX(msg)
{
	var ctrl = document.getElementById("PosCorrespondentCtrl");
	ctrl.PostMessageToSyrupStore(msg);
}


angular.module('app').factory('Connector', function() {

	var agent = navigator.userAgent.toLowerCase();

	if (agent.indexOf("windows") != -1) {
		prepareActiveXCom();
		var ocxConnector = {
			send : function(msg) {
				sendMsgThruActiveX(msg);
			},
			setListener: function(listener) {

			}
		};

		return ocxConnector;


	}else{
		var intercom = new Intercom();
		var intercomConnector = {
			send : function(msg) {
				intercom.emit('toSsa', msg);
			},
			setListener: function(listener) {
				intercom.on('toPos', listener);
			}
		};

		return intercomConnector;
	}
});