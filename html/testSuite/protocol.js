/**
 * Created by shseo on 2015-11-09.
 */
angular.module('testSuite.protocol').factory('Protocol', function() {

    var protocolVersion = "1.1.0";

    function makeTid() {
        var date = new Date();
        var tid = 'SSA' + date.getFullYear() + ('00' + (date.getMonth() + 1)).slice(-2) +
            ('00' + date.getDate()).slice(-2) + ('00' + date.getHours()).slice(-2) +
            ('00' + date.getMinutes()).slice(-2) + ('00' + date.getSeconds()).slice(-2) +
            date.getMilliseconds();

        return tid;
    };

    function makeReqSvcMsg(from, msg) {

        var protocol = {
            header: {
                transactionNumber: makeTid(),
                protocolVersion: protocolVersion,
                source: from,
                destination: "pos",
                contentType: "",
                opCode: {
                    type: "REQ",
                    command: "CM_SERVICE"
                }
            },
            body: {
                pdu: msg,
                extra:{}
            }
        };

        return protocol;
    }

    function makeReqExeMsg(from, msg) {
        var protocol = {
            header: {
                transactionNumber: makeTid(),
                protocolVersion: protocolVersion,
                source: from,
                destination: "pos",
                contentType: "",
                opCode: {
                    type: "REQ",
                    command: "CM_EXECUTE"
                }
            },
            body: {
                pdu: msg,
                extra:{}
            }
        };

        return protocol;
    }

    function makeReqMsg(from, testCase) {
        var fn = {
            'service': makeReqSvcMsg,
            'execute': makeReqExeMsg
        }
        return fn[testCase.type](from, testCase.makeJson());
    }

    return {
        makeReqMsg: makeReqMsg
    }

});
