

angular.module('testSuite.order').factory('OrderTestSuite', function($rootScope, $q, $timeout) {

    var schemeVersion = "1.2";

    var _serviceTrKey = "";
    var productName = "";
    var productCode = "";
    var productPrice = 0;
    var productQnt = 0;

    var optionName = "";
    var optionCode = "";
    var optionPrice = 0;
    var optionQnt = 0;

    var orderTestCases = {
        id: "syruporder",
        name: "Syrup Order" ,
        list: [
            {
                "name": "Basic Test",
                "cases": [
                    {
                        type: "service",
                        testName: "주문 테스트",
                        explain:"주문 번호를 생성하여 테스트를 진행한다.",
                        notice: "",
                        btn: "주문 테스트",
                        expect: [0],
                        makeJson: testOrderCheck,
                        verifier: serviceResultCheck,
                        result: '',
                        progress: false
                    },
                    {
                        type: "service",
                        testName: "주문서 재출력 테스트",
                        explain:"기존 주문 번호를 이용하여 주문서 재출력을 요청한다.",
                        notice: "",
                        btn: "주문서 재출력 요청",
                        expect: [0],
                        makeJson: testOrderReprintCheck,
                        verifier: serviceResultCheck,
                        result: '',
                        progress: false
                    },
                    {
                        type: "service",
                        testName: "주문 취소 테스트",
                        explain:"기존 주문 번호를 이용하여 주문을 취소한다.",
                        notice: "",
                        btn: "기존 주문 취소",
                        expect: [0],
                        makeJson: testOrderCancelCheck,
                        verifier: serviceResultCheck,
                        result: '',
                        progress: false
                    }
                ]
            },
            {
                name: "주문 오류 테스트",
                cases: [
                    {
                        type: "service",
                        testName: "품목 코드 오류 테스트",
                        explain:"존재하지 않는 품목 코드를 전달 한다.",
                        notice: "",
                        btn: "품목 코드 오류 테스트",
                        expect: [4001],
                        makeJson: testOrderProductCodeErrorCheck,
                        verifier: serviceResultCheck,
                        result: '',
                        progress: false
                    },
                    {
                        type: "service",
                        testName: "주문키 오류 테스트",
                        explain:"주문키 중복, null등 잘못된 주문키를 전달 한다.",
                        notice: "",
                        btn: "주문키 오류 테스트",
                        expect: [4004],
                        makeJson: testOrderTrErrorCheck,
                        verifier: serviceResultCheck,
                        result: '',
                        progress: false
                    }
                ]
            },
            {
                name: "POS에 존재 하지 않는 주문키 처리 테스트 ",
                cases: [
                    {
                        type: "service",
                        testName: "취소 오류 테스트",
                        explain:"존재하지 않는 주문번호를 이용하여 주문을 취소한다.",
                        notice: "",
                        btn: "존재하지 않는 주문 취소",
                        expect: [4003],
                        makeJson: testOrderCancelErrorCheck,
                        verifier: serviceResultCheck,
                        result: '',
                        progress: false
                    },
                    {
                        type: "service",
                        testName: "재출력 오류 테스트",
                        explain:"존재하지 않는 주문번호를 이용하여 주문서 재출력을 요청한다.",
                        notice: "",
                        btn: "존재하지 않는 주문서 재출력 요청",
                        expect: [4003],
                        makeJson: testOrderReprintErrorCheck,
                        verifier: serviceResultCheck,
                        result: '',
                        progress: false
                    }
                ]
            },
            {
                name: "프린터 장애 테스트",
                cases: [
                    {
                        type: "service",
                        testName: "주문 중 프린터 장애 테스트",
                        explain:"프린터 전원을 off 하고 주문을 요청한다.",
                        notice: "프린터 오류를 리턴하나, 주문은 정상 처리가 되어야 한다.",
                        btn: "주문 중 프린터 장애 테스트",
                        expect: [1100],
                        makeJson: testOrderPrintErrorCheck,
                        verifier: serviceResultCheck,
                        result: '',
                        progress: false
                    },
                    {
                        type: "service",
                        testName: "재출력 중 프린터 장애 테스트",
                        explain:"프린터 전원을 off 하고 주문서 재출력을 요청한다.",
                        notice: "",
                        btn: "재출력 중 프린터 장애 테스트",
                        expect: [1100],
                        makeJson: testOrderReprintPrintErrorCheck,
                        verifier: serviceResultCheck,
                        result: '',
                        progress: false
                    },
                    {
                        type: "service",
                        testName: "취소 중 프린터 장애 테스트",
                        explain:"프린터 전원을 off 하고 주문을 취소 한다.",
                        notice: "프린터 오류를 리턴하나, 취소는 정상 처리가 되어야 한다.",
                        btn: "취소 중 프린터 장애 테스트",
                        expect: [1100],
                        makeJson: testOrderCancelPrintErrorCheck,
                        verifier: serviceResultCheck,
                        result: '',
                        progress: false
                    }
                ]
            },
            {
                name: "SSA 기타 테스트",
                cases: [
                    {
                        type: "execute",
                        testName: "POS 정보 요청",
                        explain:"POS에게 기본 정보를 요청한다.",
                        notice: "SSA초기화시 요청하는 전문이며, POS는 이 요청에 반드시 응답해야 한다.",
                        btn: "POS 정보 요청",
                        expect: [0],
                        makeJson: testPOSStatusCheck,
                        verifier: executePosResultCheck,
                        result: '',
                        progress: false
                    },
                    //{
                    //    type: "execute",
                    //    testName: "POS 매장 개점/폐점 정보 대기",
                    //    explain:"POS프로그램에서 매장의 개점/폐점시 전달해야 하는 sale sales 정보를 대기한다.",
                    //    notice: "폐점의 경우 반드시 처리하지 않아도 된다.",
                    //    btn: "매장 개점/폐점 정보 대기",
                    //    expect: [0],
                    //    makeJson: testOrderReprintPrintErrorCheck,
                    //    verifier: serviceResultCheck,
                    //    result: '',
                    //    progress: false
                    //},
                    {
                        type: "service",
                        testName: "PDU 오류",
                        explain:"잘못된 PDU에 대한 처리 응답을 확인 한다.",
                        notice: "",
                        btn: "PDU 오류 테스트",
                        expect: [5],
                        makeJson: testPDUErrorCheck,
                        verifier: pduErrorResultCheck,
                        result: '',
                        progress: false
                    }
                ]
            }
        ]
    };


    var basePDU = {
        "deviceTrKey": "",
        "serviceTrKey": "",
        "serviceAliasKey": "M1",
        "serviceCode": "",
        "serviceVersion": "1.0"
    };


    function generatorKey() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }

        _serviceTrKey = s4() + s4() + '-' + s4() + '-' + s4() + '-' +s4() + '-' + s4() + s4() + s4();
    }

    function makeRePrintPDU() {

        var pdu = "";

        if(_serviceTrKey) {
            pdu = angular.copy(basePDU, {});
            pdu.serviceTrKey = _serviceTrKey;
            pdu.serviceCode = "SKP3000";
            pdu.extraData = {
                "reprint": "order"
            };
        }

        return pdu;
    }

    function makeOrderPDU() {

        var pdu = angular.copy(basePDU, {});

        pdu.serviceTrKey = _serviceTrKey;
        pdu.serviceCode = "SKP3000";
        pdu.extraData = {
            "print": "order",
            "additionalString": [
                "시럽오더 이벤트", "아메리카노 50%"
            ]
        };
        pdu.priceInfo = {
            "priceA": (productPrice  + optionPrice * 1) * 2 + productPrice,
            "priceB": (productPrice  + optionPrice * 1) * 2 + productPrice
        };
        pdu.itemList = [
            {
                "itemName": productName,
                "itemCode": productCode,
                "itemQnt": 2,
                "itemPriceA": productPrice * 2,
                "itemPriceB": productPrice * 2,
                "optionList": [
                    {
                        "itemName": optionName,
                        "itemCode": optionCode,
                        "itemQnt": 1,
                        "itemPriceA": optionPrice,
                        "itemPriceB": optionPrice
                    }
                ]
            },
            {
                "itemName": productName,
                "itemCode": productCode,
                "itemQnt": 1,
                "itemPriceA": productPrice,
                "itemPriceB": productPrice
            }
        ];

        return pdu;
    }

    function makeOrderCancelPDU() {

        var pdu = "";

        if(_serviceTrKey) {
            pdu = angular.copy(basePDU, {});

            pdu.serviceTrKey = _serviceTrKey;
            pdu.serviceCode = "SKP3001";
        }

        _serviceTrKey = ""; //취소 테스트 이후에는 주문키를 초기화 한다.
        return pdu;
    }

    function testOrderCheck() {
        // 주문 테스트
        generatorKey();
        var pdu = makeOrderPDU();
        return pdu;
    }

    function testOrderProductCodeErrorCheck() {
        // 품목코드 오류 테스트
        generatorKey();
        var pdu = makeOrderPDU();

        pdu.itemList[0].itemCode = "------";

        return pdu;
    }

    function testOrderTrErrorCheck() {
        //주문키 오류
        //동일 주문키로 주문한다.
        var pdu = makeOrderPDU();

        return pdu;
    }

    function testOrderPrintErrorCheck() {
        //주문시 프린터 장애
        return testOrderCheck();
    }

    function testOrderCancelCheck() {
        //주문 취소
        var pdu = makeOrderCancelPDU();

        if(pdu === "") {
            alert("주문내역이 없습니다.\n주문 테스트를 우선 시행해 주세요.");
        }

        return pdu;
    }

    function testOrderCancelErrorCheck() {
        //존재하지 않는 주문에 대한 취소 요청.
        generatorKey();

        return testOrderCancelCheck()
    }

    function testOrderCancelPrintErrorCheck() {
        //주문 취소시 프린터 장애
        return testOrderCancelCheck()
    }

    function testOrderReprintCheck() {
        //주문서 재출력
        var pdu = makeRePrintPDU();

        if(pdu === "") {
            alert('주문내역이 없습니다.\n주문 테스트를 우선 시행해 주세요.');
        }
        return pdu;
    }

    function testOrderReprintErrorCheck() {
        // 존재하지 않는 주문에 대한 재출력 요청 오류
        generatorKey();
        return makeRePrintPDU();
    }

    function testOrderReprintPrintErrorCheck() {

        return testOrderReprintCheck()
    }

    function testPDUErrorCheck() {
        var pdu = testOrderCheck();
        delete pdu.priceInfo;
        return pdu;
    }

    function testPOSStatusCheck() {
        var pdu = {
            "schemeVersion": schemeVersion,
            "method" : "GET",
            "scheme" : "pos/*/status"
        }
        return pdu;
    }

    function serviceResultCheck(expects, protocol) {

        var protocolError = protocol.header.responseCode.code;
        var body = protocol.body;

        if(protocolError == 0) {
            if(jQuery.inArray(body.pdu.result,expects) != -1) {
                //OK
                return {
                    result: "OK",
                    reason: JSON.stringify(protocol, null, 4)
                }
            } else {
                //NOK
                return {
                    result: "NOK",
                    reason: "result: " + body.pdu.result + "\n\n" + JSON.stringify(protocol, null, 4)
                }
            }
        } else {
            // protocol error
            return {
                result: "NOK",
                reason: "[Invalidate Protocol] protocol error: " + protocolError + "\n\n" + JSON.stringify(protocol, null, 4)
            }
        }
    }

    function pduErrorResultCheck(expects, protocol) {
        var protocolError = protocol.header.responseCode.code;

        if(jQuery.inArray(protocolError,expects)) {
            return {
                result: "OK",
                reason: JSON.stringify(protocol, null, 4)
            }
        } else {
            return {
                result: "NOK",
                reason: "result: " + protocolError + "\n\n" + JSON.stringify(protocol, null, 4)
            }
        }
    }

    function executePosResultCheck(expects, protocol) {
        var protocolError = protocol.header.responseCode.code;
        var pdu = protocol.body.pdu;

        if(protocolError == 0) {

            if(pdu.hasOwnProperty('schemeVersion') &&
                pdu.hasOwnProperty('method') &&
                pdu.hasOwnProperty('scheme') &&
                pdu.hasOwnProperty('contents') &&
                pdu.contents.hasOwnProperty('vendor') &&
                pdu.contents.hasOwnProperty('protocolVersion') &&
                pdu.contents.hasOwnProperty('schemeVersion') &&
                pdu.contents.hasOwnProperty('saleStatus')) {

                return {
                    result: "OK",
                    reason: JSON.stringify(protocol, null, 4)
                }
            } else {
                return {
                    result: "NOK",
                    reason: "result: mandatory parameter not found\n\n" + JSON.stringify(protocol, null, 4)
                }
            }
        } else {
            // protocol error
            return {
                result: "NOK",
                reason: "[Invalidate Protocol] protocol error: " + protocolError + "\n\n" + JSON.stringify(protocol, null, 4)
            }
        }
    }

    return {
        getTestList: function() {
            return orderTestCases;
        },
        setProductInfo: function(info) {
            productName = info.pname;
            productCode = info.pcode;
            productPrice = info.pprice;
            productQnt = info.pqnt;

            optionName = info.oname;
            optionCode = info.ocode;
            optionPrice = info.oprice;
            optionQnt = info.oqnt;
        }
    }
});
