/**
 * Created by shseo on 2015-11-09.
 */
'use strict'

angular.module('testSuite',
[
    'testSuite.order',
    'testSuite.benefit',
    'testSuite.protocol'
]);

angular.module('testSuite.ssa',[]);
angular.module('testSuite.order',[]);
angular.module('testSuite.benefit',[]);
angular.module('testSuite.protocol', []);