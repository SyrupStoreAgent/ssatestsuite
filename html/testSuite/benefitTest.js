/**
 * Created by shseo on 2015-11-09.
 */

angular.module('testSuite.benefit').factory('BenefitTestSuite', function() {


    var benefitTestCases = {

        id: "benefit",
        name: "혜택계열",
        list: [
            {
                "name": "거래 응답",
                "cases": [
                    {
                        type: "service",
                        testName: "OCB 적립",
                        explain:"주문 번호를 생성하여 테스트를 진행한다.",
                        notice: "",
                        btn: "주문 테스트",
                        expect: [0],
                        makeJson: saveOCB,
                        verifier: t1,
                        result: '',
                        progress: false
                    },
                    {
                        type: "service",
                        testName: "OCB 사용",
                        explain:"기존 주문 번호를 이용하여 주문서 재출력을 요청한다.",
                        notice: "",
                        btn: "주문서 재출력 요청",
                        expect: [0],
                        makeJson: useOCB,
                        verifier: t1,
                        result: '',
                        progress: false
                    },
                    {
                        type: "service",
                        testName: "Coupon 사용",
                        explain:"기존 주문 번호를 이용하여 주문을 취소한다.",
                        notice: "",
                        btn: "기존 주문 취소",
                        expect: [0],
                        makeJson: t1,
                        verifier: t1,
                        result: '',
                        progress: false
                    },
                    {
                        type: "service",
                        testName: "Mileage 적립",
                        explain:"기존 주문 번호를 이용하여 주문을 취소한다.",
                        notice: "",
                        btn: "기존 주문 취소",
                        expect: [0],
                        makeJson: t1,
                        verifier: t1,
                        result: '',
                        progress: false
                    },
                    {
                        type: "service",
                        testName: "Mileage 사용",
                        explain:"기존 주문 번호를 이용하여 주문을 취소한다.",
                        notice: "",
                        btn: "기존 주문 취소",
                        expect: [0],
                        makeJson: t1,
                        verifier: t1,
                        result: '',
                        progress: false
                    }
                ]
            },
            {
                name: "거래 취소 응답",
                cases: [
                    {
                        type: "service",
                        testName: "OCB 취소",
                        explain:"존재하지 않는 품목 코드를 전달 한다.",
                        notice: "",
                        btn: "품목 코드 오류 테스트",
                        expect: [4001],
                        makeJson: t1,
                        verifier: t1,
                        result: '',
                        progress: false
                    },
                    {
                        type: "service",
                        testName: "Coupon 취소",
                        explain:"주문키 중복, null등 잘못된 주문키를 전달 한다.",
                        notice: "",
                        btn: "주문키 오류 테스트",
                        expect: [4004],
                        makeJson: t1,
                        verifier: t1,
                        result: '',
                        progress: false
                    },
                    {
                        type: "service",
                        testName: "Mileage 취소",
                        explain:"주문키 중복, null등 잘못된 주문키를 전달 한다.",
                        notice: "",
                        btn: "주문키 오류 테스트",
                        expect: [4004],
                        makeJson: t1,
                        verifier: t1,
                        result: '',
                        progress: false
                    }
                ]
            },
            {
                name: "거래 오류 처리",
                cases: [
                    {
                        type: "service",
                        testName: "OCB 적립 오류",
                        explain:"존재하지 않는 품목 코드를 전달 한다.",
                        notice: "",
                        btn: "품목 코드 오류 테스트",
                        expect: [4001],
                        makeJson: t1,
                        verifier: t1,
                        result: '',
                        progress: false
                    },
                    {
                        type: "service",
                        testName: "Coupon 취소",
                        explain:"주문키 중복, null등 잘못된 주문키를 전달 한다.",
                        notice: "",
                        btn: "주문키 오류 테스트",
                        expect: [4004],
                        makeJson: t1,
                        verifier: t1,
                        result: '',
                        progress: false
                    },
                    {
                        type: "service",
                        testName: "Mileage 취소",
                        explain:"주문키 중복, null등 잘못된 주문키를 전달 한다.",
                        notice: "",
                        btn: "주문키 오류 테스트",
                        expect: [4004],
                        makeJson: t1,
                        verifier: t1,
                        result: '',
                        progress: false
                    }
                ]
            }
        ]
    };


    function saveOCB() {
        var pdu ={
            "deviceTrKey": "sample_pos_transactionKey....",
            "serviceTrKey": "sample_ocb_transactionKey....",
            "serviceName": "ocb",
            "serviceCode": "SKP1000",
            "serviceType": "save",
            "result": 0,
            "amount": 12000,
            "discount": 0,
            "extraData": {
                "receipt": [
                    {
                        "serviceTitle": "OK 캐쉬백",
                        "serviceCode": "SKP1000",
                        "serviceType": "save",
                        "accpoint": 3850,
                        "availpoint": 3500,
                        "currpoint": 60
                    }
                ]
            }
        }

        return pdu;
    }

    function useOCB() {

    }

    function t1() {

    }

    function t2() {

    }

    function gid() {
        return 'benefit'
    }

    return {
        getTestList: function() {
            return benefitTestCases;
        }
    }
});
