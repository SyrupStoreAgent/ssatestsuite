/**
 * Created by dc on 2015. 8. 6..
 */


angular.module('app', [
	'testSuite'
]);

angular.module('app').controller('appController', ssaController);


function ssaController($q, $scope, $timeout, Connector, Protocol, OrderTestSuite, BenefitTestSuite) {

	// Protocol Version Settings
	$scope.protocolVersion = "1.1.0";
	$scope.schemeVersion = "1.2";

	$scope.msg2Pos = "";
    $scope.responses = [];
	$scope.maxResponse = 2;
	$scope.refreshEnabled = true;

	var map = {};
	var currentTabIndex = 0;
	var hHealth = null
	$scope.healthStatus = "";
	$scope.sendMessageToPos = "";
	$scope.messageFromPos = "";

	$scope.testProgress = false;
	$scope.benefitProgress = false;

	/*
		Drop Top
		 T0000043
		 T0000114

		Cafe bene
		 HA0029
		 HA1943

		Hollys
		 2030040
		 9000001

		Dalkom
		 000090
		 000181

		Kongcha
		 1000
		 1008
		 1017
		 1033
		 1025

		Angelinus
		 ac0198
		 ae0001

		KFC
		 1245054
		 2271219

	 */
	$scope.productName = "아메리카노";
	$scope.productCode = "2030040";
	$scope.productPrice = 4000;
	$scope.productQnt = 1;

	$scope.optionName = "샷추가";
	$scope.optionCode = "9000001";
	$scope.optionPrice = 500;
	$scope.optionQnt = 1;


	$scope.groupList = [
		OrderTestSuite.getTestList(),
		BenefitTestSuite.getTestList()
	];


	$scope.makeTid = function () {
		var date = new Date();
		var tid = 'SSA' + date.getFullYear() + ('00' + (date.getMonth() + 1)).slice(-2) +
			('00' + date.getDate()).slice(-2) + ('00' + date.getHours()).slice(-2) +
			('00' + date.getMinutes()).slice(-2) + ('00' + date.getSeconds()).slice(-2) +
			date.getMilliseconds();

		return tid;
	};

	$scope.makePosTid = function () {
		var date = new Date();
		var tid = 'ssa_device_tr_key_' + date.getFullYear() + ('00' + (date.getMonth() + 1)).slice(-2) +
			('00' + date.getDate()).slice(-2) + ('00' + date.getHours()).slice(-2) +
			('00' + date.getMinutes()).slice(-2) + ('00' + date.getSeconds()).slice(-2) +
			date.getMilliseconds();

		return tid;
	};

	$scope.setCurrentTabIndex = function(idx) {
		currentTabIndex = idx;
		if($scope.groupList[idx].id === 'syruporder') {
			$scope.orderTab = true;
		} else {
			$scope.orderTab = false;
		}
	}
	
	$scope.pushResponseMessage = function(msg) { 
		if($scope.refreshEnabled) {

			if($scope.responses.length === $scope.maxResponse) {
				$scope.responses.splice($scope.maxResponse - 1, 1);
			}
			$scope.responses.splice(0, 0, msg);
		}
	};
	
	function sendMsg2Pos(msg, data) {

		$timeout(function() {
			map[msg.header.transactionNumber] = data ? data : msg;
			Connector.send(JSON.stringify(msg));
		}, 0);
	}


	function responseAliveProcess(obj) {
		var header = obj.header;
		var key = null;
		if(header.hasOwnProperty('responseCode') && header.responseCode.hasOwnProperty('code')) {

			key = header.transactionNumber;

			var alive = map[key];

			if(alive) {

				testCase.checkResult(obj)
			} else {

			}
		}
	}


	$scope.sendHealth = function() {

		var health = {
			"header" : {
				"transactionNumber": "SSA_DUMMYTRNUMBER",
				"protocolVersion": "1.1.0",
				"source": "ssa",
				"destination": "pos",
				"contentType": "",
				"opCode": {
					"type": "REQ",
					"command": "CM_ALIVE"
				}
			}
		};

		if(hHealth) {
			$timeout.cancel(hHealth);
			hHealth = null;
			$scope.healthStatus = "";
		} else {
			sendMsg2Pos(health);

			hHealth = $timeout(function() {
				$scope.sendHealth();
			}, 5000);
		}
	};


	function TestCase(g, tc) {
		var hTimer = null;
		var defer = null;
		var msg = "";
		var testCase = tc;
		var group = g;

		var createTimer = function() {
			hTimer = $timeout(function() {
				$scope.testProgress = testCase.progress = false;
				testCase.result = "NOK";
				defer.reject( {
					result: "NOK",
					reason: "Time Out!!!\n" + $scope.messageFromPos,
					tc: testCase
				});
			}, 5000);
		};
		var clearTimer = function() {
			if(hTimer) {
				$timeout.cancel(hTimer);
				hTimer = null;
			}
		};

		var init = function() {
			defer = $q.defer();
			msg = Protocol.makeReqMsg(group, testCase);
			//msg = testCase.makeJson();
		};

		this.getJson = function() {
			return msg;
		};

		this.run = function() {
			testCase.result = "";
			$scope.testProgress = testCase.progress = true;
			$scope.sendMessageToPos = JSON.stringify(msg, null, 4);

			createTimer();
			sendMsg2Pos(msg, this);
			return defer.promise;
		};

		this.checkResult = function(receiveMsg) {
			clearTimer();
			var obj = tc.verifier(tc.expect, receiveMsg);
			obj.tc = testCase;

			$scope.testProgress = testCase.progress = false;

			if(obj.result === "OK") {
				testCase.result = "OK";
				defer.resolve(obj);
			} else {
				testCase.result = "NOK";
				defer.reject(obj)
			}
		};

		init();
	}

	$scope.prepareJson = function(gidx, sgidx, item) {

		if($scope.orderTab) {
			var info = {
				pname: $scope.productName,
				pcode: $scope.productCode,
				pprice: $scope.productPrice,
				pqnt: $scope.productQnt,

				oname: $scope.optionName,
				ocode: $scope.optionCode,
				oprice: $scope.optionPrice,
				oqnt: $scope.optionQnt
			};
			OrderTestSuite.setProductInfo(info);
		}

		var group = $scope.groupList[gidx]; //order, benefit
		var subGroup = group.list[sgidx]; //sub group
		var testCase = new TestCase(name, subGroup.cases[item]);

		doTest(testCase);
	};

	function doTest(testCase) {

		var promise = testCase.run();

		promise.then(function(ok) {
			$scope.messageFromPos = ok.reason;
			//disableTest($scope.orderTestCases, ok.tc.id, false);
		}, function(nok) {
			$scope.messageFromPos = nok.reason;
			//disableTest($scope.orderTestCases, nok.tc.id, false);
		});
	}

	$scope.processMessageFromPos = function(msg) {
		var obj = null;
		var header = null;
		var body = null;

		try {
			obj = JSON.parse(msg);
			header = obj.header;
			body = obj.body;

			//$scope.messageFromPos = JSON.stringify(obj, null, 4);

			if(header && header.hasOwnProperty('transactionNumber') &&
				header.hasOwnProperty('opCode') && header.opCode.hasOwnProperty('type') && header.opCode.hasOwnProperty('command')) {

				if(header.opCode.type === 'RES') {
					switch(header.opCode.command) {
						case 'CM_ALIVE': {
							responseAliveProcess(obj);
							break;
						}
						case 'CM_EXECUTE': {
							break;
						}
						case 'CM_SERVICE': {
							responseServiceProcess(obj);
							break;
						}
						default: {

						}
					}
				} else if (header.opCode.type === 'REQ') {
					$scope.messageFromPos = "Received REQ message\n\n " + JSON.stringify(obj, null, 4);
					//혜택 계열 처리
					switch(header.opCode.command) {
						case 'CM_ALIVE': {
							break;
						}
						case 'CM_EXECUTE': {
							break;
						}
						case 'CM_SERVICE': {
							requestServiceProcess(obj);
							break;
						}
						default: {

						}
					}
				} else {
					$scope.messageFromPos = "Unknown opCode type\n\n" + JSON.stringify(obj, null, 4);
				}
			} else {
				//invalidate protocol error11
				$scope.messageFromPos = "Invalidate protocol error!!!\n\n\n" + $scope.messageFromPos;
			}
		}catch(ex) {
			$scope.messageFromPos = "JSON format error!!!\n\n\n" + msg;
			//$scope.pushResponseMessage(msg);
			return;
		}

		//if(obj.header.opCode.type === 'REQ' && obj.header.opCode.command === 'CM_ALIVE') {
		//	$scope.processCmAlive(obj);
		//	return;
		//}
		//
		//if(obj.header.opCode.type === 'REQ' && obj.header.opCode.command === 'CM_EXECUTE') {
		//	$scope.processCmExecute(obj);
		//	return;
		//}

		$scope.pushResponseMessage(msg);

	};

	Connector.setListener(function(data) {
		$scope.processMessageFromPos(data);
		$scope.$apply();
		return false;
	});

	function responseServiceProcess(obj) {

		var header = obj.header;
		var body = obj.body;
		var key = null;
		if(header.hasOwnProperty('responseCode') && header.responseCode.hasOwnProperty('code') &&
			body && body.hasOwnProperty('pdu')) {

			key = header.transactionNumber;

			var testCase = map[key];

			if(testCase) {
				testCase.checkResult(obj)
			} else {
				$scope.messageFromPos = "Invalidate transactionNumber Error!!!!\n\n" + JSON.stringify(obj, null, 4);
			}
		}
	}

	function requestSyrupProcess(obj) {
		var pdu = obj.body.pdu;

		if(pdu.serviceType === 'common') {
			$scope.benefitProgress = true;
		}
	}

	function requestServiceProcess(obj) {
		var body = obj.body;
		switch(body.pdu.serviceCode) {
			case 'SKP0000': {
				requestSyrupProcess(obj);
				break;
			}
			case 'SKP1000': {
				//ocb
				break;
			}
			case 'SKP2000': {
				//coupon
				break;
			}
			case 'SKP3000': {
				//syrup order
				break;
			}
			case 'SKP4000': {
				//mileage
				break;
			}


		}
	}

	(function init() {
		$scope.setCurrentTabIndex(currentTabIndex);
	})();
}




function prepareActiveXCom()
{
	var ctrl = document.getElementById("SSAProxyCtrl");
	ctrl.PrepareCommunication();
}

function sendMsgThruActiveX(msg)
{
	var ctrl = document.getElementById("SSAProxyCtrl");
	ctrl.PostMessageToPos(msg);
}


angular.module('app').factory('Connector', function() {

	var agent = navigator.userAgent.toLowerCase();

	if ((agent.indexOf("msie") != -1) ||
		(navigator.appName == "Netscape" && agent.indexOf("trident") != -1) /*IE 11*/) {
		prepareActiveXCom();
		var ocxConnector = {
			send : function(msg) {
				sendMsgThruActiveX(msg);
			},
			setListener: function(listener) {

			}
		};

		return ocxConnector;


	} else {
		var intercom = new Intercom();
		var intercomConnector = {
			send : function(msg) {
				intercom.emit('toPos', msg);
			},
			setListener: function(listener) {
				intercom.on('toSsa', listener);
			}
		};

		return intercomConnector;
	}

});